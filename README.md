# Revolut Money Transfer Platform

Revolut Money Transfer Platform is a dummy platform, it implement a RESTful API (including data model and the backing implementation)
for money transfers between accounts. 

## Requirements
### Explicit requirements:
1. You can use Java, Scala or Kotlin.
2. Keep it simple and to the point (e.g. no need to implement any authentication).
3. Assume the API is invoked by multiple systems and services on behalf of end users.
4. You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 – keep it simple and avoid heavy frameworks.
5. The datastore should run in-memory for the sake of this test.
6. The final result should be executable as a standalone program (should not require
a pre-installed container/server).
7. Demonstrate with tests that the API works as expected.
### Implicit requirements:
1. The code produced by you is expected to be of high quality.
2. There are no detailed requirements, use common sense.


## Plan
| Deadline | Task |
| ------ | ------ |
| 18-Jan-2019 | Analysis Requirements and Research |
| 19-Jan-2019 | Design System and Prototype Development |
| 20-Jan-2019 | Development  |
| 21-Jan-2019 | Testing |
| 22-Jan-2019 | Documentation |

## Documents

### Application Structure
![Application_Structure](./docs/application_structure.png)

### Use Case Diagram
![Use_Case_Diagram](./docs/use_case_diagram.png)

### Class Diagram
![class_diagram](./docs/class_diagram.png)

### Money Transfer Timeline Diagram
![timeline_diagram](./docs/money_transfer_timeline.png)

### Transaction Status Machine
![transaction_status_machine](./docs/transaction_status_machine.png)

## API Design
| HTTP METHOD | URI | Patameter |  Description | Response |
| -----------| ------ | ------ | ------ | ------ |
| POST | /user | { \"email\": \".\", \"firstName\": \".\", \"lastName\": \".\"} | Create an User |  |
| GET | /user/{id} | userId | get user by id |  |
| GET | /user |  | get all users |  |
| GET | /account/{id} | accountId | get account by id |  |
| GET | /account |  | get all accounts |  |
| POST | /transaction | Form Parameters: sender=1receiver=2currency=GBPamount=1.00 | Create a transaction,  transfer money(1.00) from Account 1 to 2 |  |
| GET | /transaction/{id} | transactionId | get transaction by id |  |
| GET | /transaction |  | get all transactions |  |

#### User Response Model
```
{
    "id":1,
    "dateCreated":1548097763792,
    "lastUpdated":1548097763792,
    "firstName":"name1",
    "lastName":
    "Wang1",
    "email":
    "1@test.com"
}
```
#### Account Response Model
```
{
    "id":1,
    "dateCreated":1548097763794,
    "lastUpdated":1548097763794,
    "userId":1,
    "currency":"GBP",
    "balance":100.00,
    "allowTransferIn":true,
    "allowTransferOut":true
}
```
#### Transaction Response Model
```
{
    "id":1,
    "dateCreated":1548097763795,
    "lastUpdated":1548097763795,
    "senderAccountId":2,
    "receiverAccountId":3,
    "currency":"GBP",
    "amount":100.00,
    "status":"Completed"
}
```


## Error Response
e.g.
```
{
  "code":1004,
  "message":"Id not Found."
}
```

| Internal Error Code | Description |
| ------ | ------ |
| 1000 | Sender and Receiver's Account cannot be Same. |
| 1001 | Invalid Sender Id |
| 1002 | Invalid Receiver Id |
| 1003 | Amount cannot be null and must greater than 0. |
| 1004 | Id not Found. |
| 1006 | The Account not Allow Transfer Money Out. |
| 1007 | The Account not Allow Transfer Money In. |
| 1008 | Creating Row failed in Database. |
| 1009 | Creating Row failed, no Id be generated. |
| 1010 | The Account's Currency is different. |
| 1011 | Updating Row failed in Database. |
| 1012 | Balance not enough. |

## Technology Stack
- Jersey
- Jetty
- H2 in memory database


## Quickstart
### What you’ll need
1) JDK 1.8 or later
2) Maven 3.2+

### Run the application
If you are using Maven, execute:
```
 mvn package jetty:run
```