package com.revolut.dummy.dao.h2.impl;

import com.revolut.dummy.dao.DaoFactory;
import com.revolut.dummy.dao.user.AccountDao;
import com.revolut.dummy.dao.user.TransactionDao;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by long on 20/01/2019.
 */
@RunWith(MockitoJUnitRunner.class)
public class TransactionDaoImplTest {

    private final TransactionDao transactionDao = (TransactionDao) DaoFactory.<Transaction>get(Transaction.class);
    private final AccountDao accountDao = (AccountDao) DaoFactory.<Account>get(Account.class);

    /**
     * Test case:
     *  do transaction(id = 2), which have been insert when initialize database.
     *
     */
    @Test
    public void doTransfer(){
        //given
        Transaction tran = transactionDao.findById(2);
        BigDecimal tranAmount = tran.getAmount();
        BigDecimal senderBalance = accountDao.findById(tran.getSenderAccountId()).getBalance();
        BigDecimal receiverBalance = accountDao.findById(tran.getReceiverAccountId()).getBalance();

        BigDecimal expectSenderBalance = senderBalance.subtract(tranAmount);
        BigDecimal expectReceiverBalance = receiverBalance.add(tranAmount);
        //when
        boolean result = accountDao.doTransfer(tran);

        //then
        assert result;
        Account sender = accountDao.findById(tran.getSenderAccountId());
        assert sender.getBalance().compareTo(expectSenderBalance) == 0;
        Account receiver = accountDao.findById(tran.getReceiverAccountId());
        assertEquals(receiver.getBalance(),expectReceiverBalance);
    }


    /**
     * Test Case:
     *   balance not enough, test rollback work or not.
     *
     */
    @Test
    public void doTransferWhenRollback(){
        //given
        Transaction tran = transactionDao.findById(2);
        tran.setAmount(new BigDecimal("1000000"));//not enough
        BigDecimal expectSenderBalance = accountDao.findById(tran.getSenderAccountId()).getBalance();
        BigDecimal expectReceiverBalance = accountDao.findById(tran.getReceiverAccountId()).getBalance();

        //when
        boolean result = accountDao.doTransfer(tran);

        //then
        assert !result;
        Account sender = accountDao.findById(tran.getSenderAccountId());
        assert sender.getBalance().compareTo(expectSenderBalance) == 0;
        Account receiver = accountDao.findById(tran.getReceiverAccountId());
        assertEquals(receiver.getBalance(),expectReceiverBalance);
    }

    /**
     * Test Case:
     *   Test Method setStatus of the TransactionDao
     *   it will update transaction's status if success.
     *
     */
    @Test
    public void setStatus(){
        //given
        Transaction tran = transactionDao.findById(1);

        //when
        boolean result = transactionDao.setStatus(tran.getId(), Transaction.Status.Processing);

        //then
        assert result;
        tran = transactionDao.findById(1);
        assertEquals(tran.getStatus(), Transaction.Status.Processing);
    }

}