package com.revolut.dummy.service.transaction.impl;

import com.revolut.dummy.exception.BadRequestException;
import com.revolut.dummy.model.Currency;
import com.revolut.dummy.model.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by long on 20/01/2019.
 *
 * case1:
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceImplTest {

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Before
    public void setup() {

    }

    @Test
    public void transfer() throws Exception {
        //given
        long sender = 1;
        long receiver = 2;
        Currency currency = Currency.GBP;
        BigDecimal amount = new BigDecimal(1);

        //when
        Transaction tran = transactionService.transfer(sender, receiver, currency, amount);

        //then
        assert tran != null;
        assert tran.getId() != null;
        assert tran.getSenderAccountId() == sender;
        assert tran.getReceiverAccountId() == receiver;
        assertEquals(tran.getCurrency(),currency);
        assert tran.getAmount().compareTo(amount) == 0;
        assertEquals(tran.getStatus(), Transaction.Status.Created);
    }

    /**invalid sender account id*/
    @Test(expected = BadRequestException.class)
    public void transfer1() throws Exception {
        transactionService.transfer(0, 1, Currency.GBP, new BigDecimal(1));
    }

    /**invalid receiver account id*/
    @Test(expected = BadRequestException.class)
    public void transfer2() throws Exception {
        transactionService.transfer(0, 1, Currency.GBP, new BigDecimal(1));
    }

    /** receiver account id cannot same with sender account id*/
    @Test(expected = BadRequestException.class)
    public void transfer3() throws Exception {
        transactionService.transfer(1, 1, Currency.GBP, new BigDecimal(1));
    }

    /**BigDecimal must greater than 0*/
    @Test(expected = BadRequestException.class)
    public void transfer4() throws Exception {
        transactionService.transfer(0, 1, Currency.GBP, new BigDecimal(0));
    }



}