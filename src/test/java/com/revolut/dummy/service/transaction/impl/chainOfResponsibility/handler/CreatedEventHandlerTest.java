package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.dao.h2.impl.TransactionDaoImpl;
import com.revolut.dummy.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class CreatedEventHandlerTest {

    @InjectMocks
    private CreatedEventHandler createdEventHandler;

    @InjectMocks
    private TransactionDaoImpl transactionDao;

    @Test
    public void dispatch() {
        //given
        Transaction tran = transactionDao.findById(1);

        //when
        createdEventHandler.dispatch(tran);

        //then
        tran = transactionDao.findById(1);
        assert tran != null;
        assertEquals(tran.getStatus(),Transaction.Status.Processing);
    }
}