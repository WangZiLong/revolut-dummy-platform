package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.dao.h2.impl.TransactionDaoImpl;
import com.revolut.dummy.dao.user.AccountDao;
import com.revolut.dummy.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProcessingEventHandlerTest {

    @InjectMocks
    private ProcessingEventHandler processingEventHandler;

    @InjectMocks
    private TransactionDaoImpl transactionDao;

    @Mock
    private AccountDao accountDao;

    /**
     * Test Case:
     *   do transfer success
     */
    @Test
    public void success() {
        //given
        Transaction tran = transactionDao.findById(1);
        accountDao = mock(AccountDao.class);
        when(accountDao.doTransfer(any())).thenReturn(true);
        processingEventHandler.setAccountDao(accountDao);

        //when
        processingEventHandler.dispatch(tran);

        //then
        tran = transactionDao.findById(1);
        assertEquals(tran.getStatus(), Transaction.Status.Completed);
    }

    /**
     * Test Case:
     *   do transfer failure
     */
    @Test
    public void failure() {
        //given
        Transaction tran = transactionDao.findById(1);
        accountDao = mock(AccountDao.class);
        when(accountDao.doTransfer(any())).thenReturn(false);
        processingEventHandler.setAccountDao(accountDao);

        //when
        processingEventHandler.dispatch(tran);

        //then
        tran = transactionDao.findById(1);
        assertEquals(tran.getStatus(), Transaction.Status.Failed);
    }
}