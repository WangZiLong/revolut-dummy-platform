DROP TABLE IF EXISTS User;

CREATE TABLE User (
 id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 date_created TIMESTAMP NOT NULL,
 last_updated TIMESTAMP NOT NULL,
 email VARCHAR(30) NOT NULL,
 first_name VARCHAR(255) NOT NULL,
 last_name VARCHAR(255) NOT NULL);

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (
 id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 date_created TIMESTAMP NOT NULL,
 last_updated TIMESTAMP NOT NULL,
 user_id LONG NOT NULL,
 type VARCHAR(30) NOT NULL DEFAULT 'DEBIT',
 currency VARCHAR(30) NOT NULL DEFAULT 'GBP',
 balance DECIMAL(18,2) NOT NULL DEFAULT 0.00 ,
 allow_transfer_in Boolean NOT NULL DEFAULT TRUE ,
 allow_transfer_out Boolean NOT NULL DEFAULT TRUE ,
 foreign key (user_id) references User(id));

DROP TABLE IF EXISTS Transaction;

CREATE TABLE Transaction (
 id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 date_created TIMESTAMP NOT NULL,
 last_updated TIMESTAMP NOT NULL,
 sender_account_id LONG NOT NULL,
 receiver_account_id LONG NOT NULL,
 amount DECIMAL(18,2) NOT NULL,
 status VARCHAR(30) NOT NULL DEFAULT 'Created',
 currency VARCHAR(30) NOT NULL DEFAULT 'GBP',
 foreign key (sender_account_id) references Account(id),
 foreign key (receiver_account_id) references Account(id));



INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '1@test.com', 'name1', 'Wang1');
INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '2@test.com', 'name2', 'Wang2');
INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '3@test.com', 'name3', 'Wang3');
INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '4@test.com', 'name4', 'Wang4');
INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '5@test.com', 'name5', 'Wang5');
INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '6@test.com', 'name6', 'Wang6');

INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 1,'GBP', 100);
INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 2,'GBP', 200);
INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3,'GBP', 300.01);
INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 4,'GBP', 400);
INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 5,'GBP', 500);
INSERT INTO Account (date_created, last_updated, user_id,currency, balance) VALUES (CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 6,'GBP', 600);

INSERT INTO Transaction (sender_account_id,receiver_account_id,currency,amount,status,date_created,last_updated) VALUES (1,2,'GBP',1,'Created',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
INSERT INTO Transaction (sender_account_id,receiver_account_id,currency,amount,status,date_created,last_updated) VALUES (3,4,'GBP',100,'Processing',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


