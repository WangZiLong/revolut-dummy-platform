package com.revolut.dummy.model;

import javax.persistence.Column;
import javax.persistence.Table;
import java.math.BigDecimal;

@Table(name = "Transaction")
public class Transaction extends AbstractObject{

  @Column(name="sender_account_id", nullable = false)
  private Long senderAccountId;

  @Column(name="receiver_account_id", nullable = false)
  private Long receiverAccountId;

  @Column(name="currency", nullable = false)
  private Currency currency;

  @Column(name="amount", nullable = false)
  private BigDecimal amount;

  @Column(name="status", nullable = false)
  private Status status;

  public Transaction() {
  }

  public Transaction(Long senderAccountId, Long receiverAccountId, Currency currency, BigDecimal amount, Status status) {
    this.senderAccountId = senderAccountId;
    this.receiverAccountId = receiverAccountId;
    this.currency = currency;
    this.amount = amount;
    this.status = status;
  }

  public enum Status{
    Created,//The transaction has be created and it will be processed shortly.
    Processing,//We’re processing your the transaction should be completed shortly.
    Completed,//The transaction was successful and the money is in the recipient’s account.
    Failed,//Your payment didn’t go through. We recommend that you try your payment again.
//    Refused,//The recipient didn’t receive your payment.
//    Refunded,//The recipient returned your payment.
//    Canceled,//You canceled your payment, and the money will back to your account.
  }

  public Long getSenderAccountId() {
    return senderAccountId;
  }

  public void setSenderAccountId(Long senderAccountId) {
    this.senderAccountId = senderAccountId;
  }

  public Long getReceiverAccountId() {
    return receiverAccountId;
  }

  public void setReceiverAccountId(Long receiverAccountId) {
    this.receiverAccountId = receiverAccountId;
  }

  public Currency getCurrency() {
    return currency;
  }

  public void setCurrency(Currency currency) {
    this.currency = currency;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}