package com.revolut.dummy.model;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.util.Date;

/**
 * Abstract Object
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public abstract class AbstractObject implements Serializable, Cloneable{
    @Column(name="id")
    protected Long id;
    @Column(name="date_created")
    protected Date dateCreated;
    @Column(name="last_updated")
    protected Date lastUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

}
