package com.revolut.dummy.model;

import javax.persistence.Column;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

@Table(name = "Account")
public class Account extends AbstractObject{

  @Column(name="user_id", nullable = false)
  private Long userId;

    @Column(name="currency", nullable = false)
    private Currency currency;

  @Column(name="balance", nullable = false)
  private BigDecimal balance;

  @Column(name="allow_transfer_in", nullable = false)
  private Boolean allowTransferIn;

  @Column(name="allow_transfer_out", nullable = false)
  private Boolean allowTransferOut;

//  @Column(name="is_active")
//  private Boolean isActive;
//
//  @Column(name="is_freezed")
//  private Boolean isFreezed;


  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public Boolean getAllowTransferIn() {
    return allowTransferIn;
  }

  public void setAllowTransferIn(Boolean allowTransferIn) {
    this.allowTransferIn = allowTransferIn;
  }

  public Boolean getAllowTransferOut() {
    return allowTransferOut;
  }

  public void setAllowTransferOut(Boolean allowTransferOut) {
    this.allowTransferOut = allowTransferOut;
  }

}