package com.revolut.dummy.exception;

public class BadRequestException extends CustomException{

  public BadRequestException(ApiResponse.Status status) {
    super(status);
  }
}
