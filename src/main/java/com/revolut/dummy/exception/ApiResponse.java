package com.revolut.dummy.exception;

@javax.xml.bind.annotation.XmlRootElement
public class ApiResponse {
  private int code;
  private String message;
  
  public ApiResponse(){}
  
  public ApiResponse(int code, String message){
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public enum Status {
    SENDER_RECEIVER_ID_CANNOT_SAME(1000, "Sender and Receiver's Account cannot be Same"),
    INVALID_SENDER_ID(1001, "Invalid Sender Id"),
    INVALID_RECEIVER_ID(1002, "Invalid Receiver Id"),
    INVALID_AMOUNT_WHEN_MONEY_TRANSFER(1003, "Amount cannot be null and must greater than 0."),
    ID_NOT_FOUND(1004, "Id not Found."),
    NOT_ALLOW_TRANSFER_OUT(1006, "The Account not Allow Transfer Money Out."),
    NOT_ALLOW_TRANSFER_IN(1007, "The Account not Allow Transfer Money In."),
    CREATE_ROW_IN_DB(1008, "Creating Row failed in Database."),
    NO_ID_BE_GENERATED(1009, "Creating Row failed, no Id be generated."),
    INVALID_ACCOUNT_CURRENCY(1010, "The Account's Currency is different."),
    ERROR_UPDATE_ROW_IN_DB(1011, "Updating Row failed in Database."),
    BALANCE_NOT_ENOUGH(1012, "Balance not enough."),

    ;

    private final int code;//internal error code.
    private final String message;

    public int getCode() {
      return code;
    }

    public String getMessage() {
      return message;
    }

    Status(final int statusCode, final String message) {
      this.code = statusCode;
      this.message = message;
    }
  }
}
