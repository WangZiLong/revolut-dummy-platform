package com.revolut.dummy.exception;

public class CustomException extends RuntimeException {

    private final int error;

    private final String message;


    public CustomException(ApiResponse.Status status) {
        this(status.getCode(), status.getMessage());
    }

    public CustomException(int error, String message) {
        super(message);
        this.message = message;
        this.error = error;
    }

    public int getError() {
        return error;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
