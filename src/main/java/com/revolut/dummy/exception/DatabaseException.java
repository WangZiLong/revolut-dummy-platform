package com.revolut.dummy.exception;

public class DatabaseException extends CustomException {
  public DatabaseException(ApiResponse.Status status) {
    super(status);
  }
}
