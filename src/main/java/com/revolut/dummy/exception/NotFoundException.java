package com.revolut.dummy.exception;

public class NotFoundException extends CustomException{

  public NotFoundException(ApiResponse.Status status) {
    super(status);
  }
}
