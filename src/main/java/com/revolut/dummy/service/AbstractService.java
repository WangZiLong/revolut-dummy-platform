package com.revolut.dummy.service;


import com.revolut.dummy.dao.Dao;
import com.revolut.dummy.dao.DaoFactory;
import com.revolut.dummy.exception.NotFoundException;
import com.revolut.dummy.model.AbstractObject;

import java.util.List;

import static com.revolut.dummy.exception.ApiResponse.Status.ID_NOT_FOUND;

public abstract class AbstractService<T extends AbstractObject> implements Service<T> {
    protected Dao<T> dao;

    private final Class<T> clazz;

    public AbstractService(Class<T> clazz) {
        this.clazz = clazz;
        this.dao = DaoFactory.get(clazz);
    }


    @Override
    public T findById(final long id) {
        T t = dao.findById(id);
        if (t == null)
            throw new NotFoundException(ID_NOT_FOUND);
        return t;
    }

    @Override
    public long create(T t) {
        return dao.create(t);
    }

    @Override
    public List<T> findAll(Integer offset, Integer limit) {
        return dao.findAll(offset, limit);
    }
}
