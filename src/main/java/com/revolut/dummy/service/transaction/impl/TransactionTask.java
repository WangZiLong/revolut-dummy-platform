package com.revolut.dummy.service.transaction.impl;

import com.revolut.dummy.dao.DaoFactory;
import com.revolut.dummy.dao.user.TransactionDao;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.ChainFactory;

import java.util.logging.Logger;

/**
 * Created by long on 20/01/2019.
 * <p>
 * Feature:
 * background task to consume transaction.
 */
class TransactionTask implements Runnable {

    private static final Logger logger = Logger.getLogger(TransactionTask.class.getName());

    private long transactionId;

    private final TransactionDao transactionDao = (TransactionDao) DaoFactory.<Transaction>get(Transaction.class);

    public TransactionTask(long transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public void run() {
        logger.info("TransactionTask run ... & id: " + transactionId);

        Transaction tran = transactionDao.findById(transactionId);
        if (tran == null) {
            logger.info("TransactionTask id not found. id:" + transactionId);
            return;
        }

        ChainFactory.getInstance().doChain(tran);

    }
}
