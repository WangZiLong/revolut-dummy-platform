package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.model.Transaction;

/**
 * Created by long on 20/01/2019.
 */
public interface EventHandler {

    /**
     * need to do dispatch or not
     *
     * @param: transaction
     *
     * return boolean
     *
     */
    boolean onPreHandle(final Transaction tran);

    void dispatch(final Transaction tran);
}
