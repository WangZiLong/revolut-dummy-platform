package com.revolut.dummy.service.transaction.impl;
    
import com.revolut.dummy.dao.DaoFactory;
import com.revolut.dummy.dao.user.AccountDao;
import com.revolut.dummy.exception.BadRequestException;
import com.revolut.dummy.exception.NotFoundException;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Currency;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.service.AbstractService;
import com.revolut.dummy.service.transaction.TransactionService;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static com.revolut.dummy.exception.ApiResponse.Status.*;


public class TransactionServiceImpl extends AbstractService<Transaction> implements TransactionService {

    public TransactionServiceImpl() { super(Transaction.class); }

    private static final Logger logger = Logger.getLogger(TransactionServiceImpl.class.getName());

    private final AccountDao accountDao = (AccountDao) DaoFactory.<Account>get(Account.class);

    private ExecutorService executor = Executors.newFixedThreadPool(4);


    /**
     * Transfer Money
     *
     * 1. check parameter
     *      sender != receiver & not null
     *      sender allowTransferOut true
     *      receiver allowTransferIn true
     *      amount > 0,
     *      amount <= sender.availableBalance
     *
     * 2. create transaction
     *
     * 3. start a task to process the transaction
     *
     * 4. return
     *
     * @param sender: sender's account id
     * @param receiver: receiver's account id
     * @param amount: How much money need be transfer
     *
     * return: Transaction Information.
     *
     */
    @Override
    public Transaction transfer(final long sender, final long receiver, final Currency currency, @NotNull final BigDecimal amount) {

        checkBeforeTransfer(sender, receiver, currency, amount);
        logger.info("transfer money from " + sender + " to " + receiver + " amount " + amount.toString());

        //create transaction
        Transaction tran = new Transaction(sender, receiver, currency, amount, Transaction.Status.Created);
        long id = dao.create(tran);
        tran.setId(id);

        //create a background task to do transaction.
        executor.submit(new TransactionTask(id));

        logger.info("Creating Transaction Success, id: " + id);
        return tran;
    }


    private void checkBeforeTransfer(final long sender, final long receiver,final Currency currency, final BigDecimal amount){
        if (sender <= 0)
            throw new BadRequestException(INVALID_SENDER_ID);
        if (receiver <= 0)
            throw new BadRequestException(INVALID_RECEIVER_ID);
        if (amount == null || amount.doubleValue() <= 0)
            throw new BadRequestException(INVALID_AMOUNT_WHEN_MONEY_TRANSFER);
        if (sender == receiver)
            throw new BadRequestException(SENDER_RECEIVER_ID_CANNOT_SAME);

        //check sender's account info
        Account senderAccount = accountDao.findById(sender);
        if (senderAccount == null)
            throw new NotFoundException(ID_NOT_FOUND);

        //check sender's account allow to transfer out or not.
        if (!senderAccount.getAllowTransferOut())
            throw new BadRequestException(NOT_ALLOW_TRANSFER_OUT);
        //check balance is enough or not
        if (senderAccount.getBalance().compareTo(amount) < 0)
            throw new BadRequestException(BALANCE_NOT_ENOUGH);
        //check currency
        if (!senderAccount.getCurrency().equals(currency))
            throw new BadRequestException(INVALID_ACCOUNT_CURRENCY);

        //check receiver's account info
        Account receiverAccount = accountDao.findById(receiver);
        if (receiverAccount == null)
            throw new NotFoundException(ID_NOT_FOUND);

        //check receiver account allow to transfer in or not.
        if (!receiverAccount.getAllowTransferIn())
            throw new BadRequestException(NOT_ALLOW_TRANSFER_IN);

        //check currency
        if (!receiverAccount.getCurrency().equals(currency))
            throw new BadRequestException(INVALID_ACCOUNT_CURRENCY);

    }
}
