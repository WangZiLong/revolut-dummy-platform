package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.model.Transaction;

import java.util.logging.Logger;

/**
 * Created by long on 20/01/2019.
 *
 * 1. start database transaction
 * 2. get sender&receiver's account
 * 3. check balance and account status
 * 4. freeze balance of sender's account
 *
 * Reason:
 *   sometimes sender's account and receiver's account
 *
 *   in different database.
 *
 *   So we cannot update balance them in one database transaction.
 *
 */
public class CreatedEventHandler extends BaseEventHandler{

    private static final Logger logger = Logger.getLogger(CreatedEventHandler.class.getName());

    public CreatedEventHandler() {
        super(Transaction.Status.Created);
    }

    @Override
    public void dispatch(Transaction transaction) {
        logger.info("CreatedEventHandler start, id: " + transaction.getId());

        //todo: review the transaction or freeze the balance in next version.
        //in last version, just set the status of transaction as Processing.
        if(transactionDao.setStatus(transaction.getId(), Transaction.Status.Processing)){
            transaction.setStatus(Transaction.Status.Processing);
            logger.info(" Success, Transaction Status from Created -> Processing, id: " + transaction.getId());
        }

    }
}
