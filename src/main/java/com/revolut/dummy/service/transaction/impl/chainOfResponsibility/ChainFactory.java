package com.revolut.dummy.service.transaction.impl.chainOfResponsibility;

import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler.CompletedEventHandler;
import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler.CreatedEventHandler;
import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler.FailedEventHandler;
import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler.ProcessingEventHandler;

/**
 * Created by long on 20/01/2019.
 *
 */
public class ChainFactory {

    private static Chain chain;

    public static Chain getInstance(){
        if (chain == null){
            synchronized (ChainFactory.class){
                if (chain == null){
                    onCreated();
                }
            }
        }
        return chain;
    }


    /**
     * Creating Chain of the Transaction's status
     *
     * e.g.
     *     Created -> Processing -> Completed -> Failed
     *
     */
    private static void onCreated(){
        TransactionChain tcCreated = new TransactionChain(new CreatedEventHandler());
        TransactionChain tcProcessing = new TransactionChain(new ProcessingEventHandler());
        TransactionChain tcCompleted = new TransactionChain(new CompletedEventHandler());
        TransactionChain tcFailed = new TransactionChain(new FailedEventHandler());

        tcCreated.setNext(tcProcessing);
        tcProcessing.setNext(tcCompleted);
        tcCompleted.setNext(tcFailed);

        chain = tcCreated;
    }
}
