package com.revolut.dummy.service.transaction;


import com.revolut.dummy.model.Currency;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.service.Service;

import java.math.BigDecimal;

public interface TransactionService extends Service<Transaction> {

    /**
     * Transfer Money from Sender's account to Receiver's account
     *
     * @param sender: sender's account id
     * @param receiver: receiver's account id
     * @param amount: How much money need be transfer
     *
     * return: Transaction Information.
     *
     */
    Transaction transfer(long sender, long receiver, final Currency currency, BigDecimal amount);
}