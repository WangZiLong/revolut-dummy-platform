package com.revolut.dummy.service.transaction.impl.chainOfResponsibility;

import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler.EventHandler;

/**
 * Created by long on 20/01/2019.
 */
public class TransactionChain implements Chain {

    private EventHandler handler;
    private Chain next;

    public TransactionChain(EventHandler handler) {
        this.handler = handler;
    }

    public void setNext(Chain next) {
        this.next = next;
    }

    @Override
    public void doChain(Transaction transaction) {
        if (handler.onPreHandle(transaction))
            handler.dispatch(transaction);

        if (next != null)
            next.doChain(transaction);
    }
}
