package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.dao.DaoFactory;
import com.revolut.dummy.dao.user.AccountDao;
import com.revolut.dummy.dao.user.TransactionDao;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;

import javax.annotation.Resource;

/**
 * Created by long on 20/01/2019.
 */
public abstract class BaseEventHandler implements EventHandler{

    protected Transaction.Status status;

    protected TransactionDao transactionDao = (TransactionDao) DaoFactory.<Transaction>get(Transaction.class);
    protected AccountDao accountDao = (AccountDao) DaoFactory.<Account>get(Account.class);

    BaseEventHandler(Transaction.Status status) {
        this.status = status;
    }

    public void setAccountDao(AccountDao accountDao){
        this.accountDao = accountDao;
    }

    @Override
    public boolean onPreHandle(Transaction Transaction) {
        return Transaction.getStatus().equals(status);
    }

}