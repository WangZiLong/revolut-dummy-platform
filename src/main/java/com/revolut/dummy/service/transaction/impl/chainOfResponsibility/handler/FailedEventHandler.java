package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.model.Transaction;

import java.util.logging.Logger;

/**
 * Created by long on 20/01/2019.
 *
 */
public class FailedEventHandler extends BaseEventHandler{

    private static final Logger logger = Logger.getLogger(CreatedEventHandler.class.getName());

    public FailedEventHandler() {
        super(Transaction.Status.Failed);
    }

    @Override
    public void dispatch(Transaction transaction) {
        logger.info("FailedEventHandler start, id: " + transaction.getId());
        //todo: check transaction and do rollback

    }
}
