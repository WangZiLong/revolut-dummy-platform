package com.revolut.dummy.service.transaction.impl.chainOfResponsibility;

import com.revolut.dummy.model.Transaction;

/**
 * Created by long on 20/01/2019.
 *
 */
public interface Chain {

    /**
     *
     * @param: transaction
     */
    void doChain(final Transaction transaction);
}
