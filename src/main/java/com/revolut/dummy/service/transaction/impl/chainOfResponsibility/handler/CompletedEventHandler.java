package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.model.Transaction;

import java.util.logging.Logger;

/**
 * Created by long on 20/01/2019.
 * 
 */
public class CompletedEventHandler extends BaseEventHandler{

    private static final Logger logger = Logger.getLogger(CreatedEventHandler.class.getName());

    public CompletedEventHandler() {
        super(Transaction.Status.Completed);
    }

    @Override
    public void dispatch(Transaction transaction) {
        logger.info("CompletedEventHandler start, id: " + transaction.getId());
    }
}
