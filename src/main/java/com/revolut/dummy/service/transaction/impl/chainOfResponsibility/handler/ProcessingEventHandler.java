package com.revolut.dummy.service.transaction.impl.chainOfResponsibility.handler;

import com.revolut.dummy.model.Transaction;

import java.util.logging.Logger;

/**
 * Created by long on 20/01/2019.
 *
 * Handle the transaction When its status is Processing.
 *
 * Do transfer money
 *
 * if success set status to Completed
 *
 * if failure set status to Failed.
 *
 */
public class ProcessingEventHandler extends BaseEventHandler{

    private static final Logger logger = Logger.getLogger(CreatedEventHandler.class.getName());

    public ProcessingEventHandler() {
        super(Transaction.Status.Processing);
    }

    private static final Transaction.Status STATUS_ON_SUCCESS = Transaction.Status.Completed;
    private static final Transaction.Status STATUS_ON_FAILURE = Transaction.Status.Failed;

    @Override
    public void dispatch(Transaction transaction) {
        logger.info("ProcessingEventHandler start, id: " + transaction.getId());

        if (accountDao.doTransfer(transaction)){
            transactionDao.setStatus(transaction.getId(), STATUS_ON_SUCCESS);
            transaction.setStatus(STATUS_ON_SUCCESS);
            logger.info("Success,Transaction status -> Completed, id: " + transaction.getId());
        }else {
            transactionDao.setStatus(transaction.getId(), STATUS_ON_FAILURE);
            transaction.setStatus(STATUS_ON_FAILURE);
            logger.info("Failure,Transaction status -> Failed, id: " + transaction.getId());
        }
    }
}
