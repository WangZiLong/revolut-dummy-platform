package com.revolut.dummy.service.user;


import com.revolut.dummy.model.User;
import com.revolut.dummy.service.Service;

public interface UserService extends Service<User> {

}