package com.revolut.dummy.service.user.impl;

import com.revolut.dummy.model.User;
import com.revolut.dummy.service.AbstractService;
import com.revolut.dummy.service.user.UserService;

import java.util.logging.Logger;


public class UserServiceImpl extends AbstractService<User> implements UserService {

    public UserServiceImpl() { super(User.class); }

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class.getName());

}
