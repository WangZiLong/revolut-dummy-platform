package com.revolut.dummy.service.account.impl;

import com.revolut.dummy.model.Account;
import com.revolut.dummy.service.AbstractService;
import com.revolut.dummy.service.account.AccountService;
import java.util.logging.Logger;


public class AccountServiceImpl extends AbstractService<Account> implements AccountService {

    public AccountServiceImpl() { super(Account.class); }

    private static final Logger logger = Logger.getLogger(AccountServiceImpl.class.getName());

}
