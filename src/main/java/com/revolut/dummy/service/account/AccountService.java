package com.revolut.dummy.service.account;


import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.User;
import com.revolut.dummy.service.Service;

public interface AccountService extends Service<Account> {

}