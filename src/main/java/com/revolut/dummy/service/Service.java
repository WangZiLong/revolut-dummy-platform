package com.revolut.dummy.service;

import java.util.List;

public interface Service<T> {

    /**
     * Get resource by id
     *
     * @param id: resource id
     * @return
     */
    T findById(final long id);

    /**
     * Create a resource
     *
     * @param t:
     * @return
     */
    long create(T t);

    /**
     * List all resources
     *
     * @param offset:
     * @param limit:
     * @return
     */
    List<T> findAll(Integer offset, Integer limit);
}
