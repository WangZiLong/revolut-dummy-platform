package com.revolut.dummy.service;

import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.model.User;
import com.revolut.dummy.service.account.impl.AccountServiceImpl;
import com.revolut.dummy.service.transaction.impl.TransactionServiceImpl;
import com.revolut.dummy.service.user.impl.UserServiceImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Service Factory
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public class ServiceFactory {

    private static final Map<Class, Service> cache = new HashMap<>();

    static {
        cache.put(User.class, new UserServiceImpl());
        cache.put(Account.class, new AccountServiceImpl());
        cache.put(Transaction.class, new TransactionServiceImpl());
    }

    public static <T> Service<T> get(Class clazz){
        return cache.get(clazz);
    }

}
