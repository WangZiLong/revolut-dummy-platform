package com.revolut.dummy.resource;

import com.revolut.dummy.model.Account;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource extends AbstractResource<Account> {

    public AccountResource() {
        this(Account.class);
    }

    public AccountResource(Class<Account> clazz) {
        super(clazz);
    }


    /**
     * Get Account by id
     */
    @GET
    @Path("/{id}")
    public Account get(@PathParam("id") final long id) {
        return service.findById(id);
    }

    /**
     * Get All Accounts
     */
    @GET
    @Path("/")
    public List<Account> getAll(@QueryParam("offset") Integer offset,
                             @QueryParam("limit") Integer limit) {
        if (offset == null)
            offset = 0;
        if (limit == null)
            limit = 100;
        return service.findAll(offset, limit);
    }

}
