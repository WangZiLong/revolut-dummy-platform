package com.revolut.dummy.resource;

import com.revolut.dummy.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource extends AbstractResource<User> {

    public UserResource() {
        this(User.class);
    }

    public UserResource(Class<User> clazz) {
        super(clazz);
    }

    /**
     * Create an User
     */
    @POST
    @Path("/")
    public Response post(User user) {
        long userId = service.create(user);
        return Response.ok()
                .entity(userId)
                .build();
    }

    /**
     * Get user by userId
     */
    @GET
    @Path("/{id}")
    public User get(@PathParam("id") final long id) {
        return service.findById(id);
    }

    /**
     * Get All Users
     */
    @GET
    @Path("/")
    public List<User> getAll(@QueryParam("offset") Integer offset,
                             @QueryParam("limit") Integer limit) {
        if (offset == null)
            offset = 0;
        if (limit == null)
            limit = 100;
        return service.findAll(offset, limit);
    }

}
