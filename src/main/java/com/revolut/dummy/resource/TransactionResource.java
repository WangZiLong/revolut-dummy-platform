package com.revolut.dummy.resource;

import com.revolut.dummy.model.Currency;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.service.ServiceFactory;
import com.revolut.dummy.service.transaction.TransactionService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.List;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource extends AbstractResource<Transaction> {

    public TransactionResource() {
        this(Transaction.class);
    }

    public TransactionResource(Class<Transaction> clazz) {
        super(clazz);
    }

    //todo: @Inject instead of Factory
    private TransactionService transactionService = (TransactionService) ServiceFactory.<Transaction>get(Transaction.class);


    /**
     * Get Transaction by id
     */
    @GET
    @Path("/{id}")
    public Transaction get(@PathParam("id") final long id) {
        return service.findById(id);
    }



    /**
     * Create Transaction
     *
     * Transfer Money
     *
     * todo: async api.
     */
    @POST
    @Path("/")
    public Transaction transfer(@FormParam("sender") final long sender,
                                @FormParam("receiver") final long receiver,
                                @FormParam("currency") Currency currency,
                                @FormParam("amount") final BigDecimal amount) {
        if (currency == null)
            currency = Currency.GBP;
        return transactionService.transfer(sender, receiver,currency, amount);
    }

    /**
     * Get All Transactions
     */
    @GET
    @Path("/")
    public List<Transaction> getAll(@QueryParam("offset") Integer offset,
                             @QueryParam("limit") Integer limit) {
        if (offset == null)
            offset = 0;
        if (limit == null)
            limit = 100;
        return service.findAll(offset, limit);
    }

}
