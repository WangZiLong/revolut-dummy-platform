package com.revolut.dummy.resource;


import com.revolut.dummy.service.Service;
import com.revolut.dummy.service.ServiceFactory;

/**
 * Abstract Resource
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public abstract class AbstractResource<T> {

    protected Service<T> service;

    private final Class<T> clazz;

    public AbstractResource(Class<T> clazz) {
        this.clazz = clazz;
        this.service = ServiceFactory.get(clazz);
    }


}
