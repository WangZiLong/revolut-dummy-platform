package com.revolut.dummy.dao.h2.database;

import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;


/**
 * H2 Database Manager.
 *
 * @author Zilong.Wang
 */
public class H2 {

    private static final String DRIVE = "org.h2.Driver";
    private static final String DB_NAME = "test";
    private static final String CONNECTION_URL = "jdbc:h2:mem:" + DB_NAME + ";INIT=RUNSCRIPT FROM 'classpath:schema.sql';DB_CLOSE_DELAY=-1";
    private static final String USER = "sa";
    private static final String PASSWORD = "sa";

    private static JdbcConnectionPool cp = null;


    /**
     * get a h2 connection from Connection Pool.
     *
     * @exception SQLException if a database access error occurs
     * @return a Connection object
     */
    public static Connection getConnection() throws SQLException {
        if (cp == null) {
            synchronized (H2.class) {
                if (cp == null) {
                    cp = JdbcConnectionPool.create(CONNECTION_URL, USER, PASSWORD);
                }
            }
        }
        return cp.getConnection();
    }
}
