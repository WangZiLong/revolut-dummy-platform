package com.revolut.dummy.dao.h2.impl;

import com.revolut.dummy.dao.h2.AbstractDao;
import com.revolut.dummy.dao.h2.database.H2;
import com.revolut.dummy.dao.user.TransactionDao;
import com.revolut.dummy.exception.DatabaseException;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import org.apache.commons.dbutils.DbUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import static com.revolut.dummy.exception.ApiResponse.Status.CREATE_ROW_IN_DB;
import static com.revolut.dummy.exception.ApiResponse.Status.ERROR_UPDATE_ROW_IN_DB;


/**
 * Transaction Dao implement
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public class TransactionDaoImpl extends AbstractDao<Transaction> implements TransactionDao{

  private static final Logger logger = Logger.getLogger(TransactionDaoImpl.class.getName());

  public TransactionDaoImpl() {
    super(Transaction.class);
  }

  /**
   * Update transaction's status by id.
   *
   * @param id: transaction id
   * @param status: Transaction status
   * @return boolean
   */
    @Override
    public boolean setStatus(long id, Transaction.Status status) {
      Connection conn = null;
      PreparedStatement ps = null;
      ResultSet rs = null;
      try {
        conn = H2.getConnection();
        ps = conn.prepareStatement("UPDATE Transaction SET status = ? where id = ?");
        ps.setString(1, status.name());
        ps.setLong(2, id);
        int  rowsUpdated = ps.executeUpdate();
        if (rowsUpdated == 0) {
          logger.severe("Update Transaction Row Failure.");
          throw new DatabaseException(ERROR_UPDATE_ROW_IN_DB);
        }
        conn.commit();
        return true;
      } catch (Exception e) {
        e.printStackTrace();
      }finally {
        close(rs,ps,conn);
      }
      return false;
    }
}