package com.revolut.dummy.dao.h2.impl;

import com.revolut.dummy.dao.h2.AbstractDao;
import com.revolut.dummy.dao.user.UserDao;
import com.revolut.dummy.model.User;

import java.util.logging.Logger;


/**
 * User Dao implement
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public class UserDaoImpl extends AbstractDao<User> implements UserDao{

  private static final Logger logger = Logger.getLogger(UserDaoImpl.class.getName());

  public UserDaoImpl() {
    super(User.class);
  }

}