package com.revolut.dummy.dao.h2.impl;

import com.revolut.dummy.dao.h2.AbstractDao;
import com.revolut.dummy.dao.h2.database.H2;
import com.revolut.dummy.dao.user.AccountDao;
import com.revolut.dummy.exception.DatabaseException;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import org.apache.commons.dbutils.DbUtils;

import java.io.Closeable;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

import static com.revolut.dummy.exception.ApiResponse.Status.*;


/**
 * Account Dao implement
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public class AccountDaoImpl extends AbstractDao<Account> implements AccountDao{

  private static final Logger logger = Logger.getLogger(AccountDaoImpl.class.getName());

  public AccountDaoImpl() {
    super(Account.class);
  }

  private final static String UPDATE_BALANCE_BY_ID = "UPDATE Account SET balance = ? where id = ?";

  /**
   * Transfer Money from Sender's account to Receiver's Account
   *
   *  1. get database connection
   *  2. close auto commit
   *  3. get sender's account and check account's info
   *  4. get receiver's account and check account's info
   *  5. Update both account
   *  6. return
   *
   * @param transaction: Trsaction
   * @return transfer success or not.
   */
  @Override
  public boolean doTransfer(Transaction transaction) {
    Connection conn = null;
    PreparedStatement ps1 = null;
    PreparedStatement ps2 = null;
    PreparedStatement ps3 = null;
    PreparedStatement ps4 = null;
    Account senderAccount = null;
    Account receiverAccount = null;
    ResultSet rs = null;
    try {
      conn = H2.getConnection();
      boolean flag = conn.getAutoCommit();
      conn.setAutoCommit(false);

      //select sender's account
      ps1 = conn.prepareStatement(sql_select_by_id);
      ps1.setLong(1, transaction.getSenderAccountId());
      rs = ps1.executeQuery();
      if (rs.next()){
        senderAccount = newInstance(rs);
      }

      //double check sender's account
      if (senderAccount == null)
        throw new DatabaseException(ID_NOT_FOUND);
      if (senderAccount.getBalance().compareTo(transaction.getAmount()) < 0)
        throw new DatabaseException(BALANCE_NOT_ENOUGH);
      if (!senderAccount.getAllowTransferOut())
        throw new DatabaseException(NOT_ALLOW_TRANSFER_OUT);
      if (!senderAccount.getCurrency().equals(transaction.getCurrency()))
        throw new DatabaseException(INVALID_ACCOUNT_CURRENCY);

      //select receiver's account
      ps2 = conn.prepareStatement(sql_select_by_id);
      ps2.setLong(1, transaction.getReceiverAccountId());
      rs = ps2.executeQuery();
      if (rs.next()){
        receiverAccount = newInstance(rs);
      }

      //double check receiver's account
      if (receiverAccount == null)
        throw new DatabaseException(ID_NOT_FOUND);
      if (!receiverAccount.getAllowTransferIn())
        throw new DatabaseException(NOT_ALLOW_TRANSFER_IN);
      if (!receiverAccount.getCurrency().equals(transaction.getCurrency()))
        throw new DatabaseException(INVALID_ACCOUNT_CURRENCY);

      //update sender's account
      ps3 = conn.prepareStatement(UPDATE_BALANCE_BY_ID);
      ps3.setBigDecimal(1,senderAccount.getBalance().subtract(transaction.getAmount()));
      ps3.setLong(2, senderAccount.getId());
      ps3.executeUpdate();

      //update receiver's account
      ps4 = conn.prepareStatement(UPDATE_BALANCE_BY_ID);
      ps4.setBigDecimal(1,receiverAccount.getBalance().add(transaction.getAmount()));
      ps4.setLong(2, receiverAccount.getId());
      ps4.executeUpdate();

      conn.setAutoCommit(flag);
      conn.commit();
      return true;
    } catch (Exception e) {
      e.printStackTrace();

      //if any exception, do rollback.
      try {
        conn.rollback();
      } catch (SQLException e1) {
        e1.printStackTrace();
      }
    } finally {
      close(rs, ps1, ps2, ps3, ps4, conn);
    }

    return false;
  }
}