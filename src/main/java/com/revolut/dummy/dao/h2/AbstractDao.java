
package com.revolut.dummy.dao.h2;

import com.revolut.dummy.dao.Dao;
import com.revolut.dummy.dao.h2.database.H2;
import com.revolut.dummy.exception.DatabaseException;
import com.revolut.dummy.model.AbstractObject;
import org.apache.commons.beanutils.BeanUtils;

import javax.persistence.Column;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.logging.Logger;

import static com.revolut.dummy.exception.ApiResponse.Status.CREATE_ROW_IN_DB;
import static com.revolut.dummy.exception.ApiResponse.Status.NO_ID_BE_GENERATED;

/**
 * H2 Abstract DAO
 *
 * @author Zilong.Wang
 *         <p>
 *         19-01-2019
 */
public abstract class AbstractDao<T extends AbstractObject> implements Dao<T> {

    private static final Logger logger = Logger.getLogger(AbstractDao.class.getName());

    private final Class<T> clazz;

    protected final String tableName;         //table name: User
    protected final String sql_select_by_id;  //SELECT * FROM User WHERE id = 1
    protected final String sql_select_all;    //SELECT * FROM User OFFSET 0 LIMIT 100
    protected String sql_insert;    //INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (?, ?, ?, ?, ?)

    public AbstractDao(Class<T> clazz) {
        this.clazz = clazz;
        tableName = tableName(clazz);
        sql_select_by_id = "SELECT * FROM " + tableName + " WHERE id = ?";
        sql_select_all = "SELECT * FROM " + tableName + " OFFSET ? LIMIT ?";
        sql_insert = "INSERT INTO " + tableName + " (";
    }

    /**
     * Create a row in database
     *
     * @param t:
     * @return: generated id
     */
    public long create(T t){
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            //update datetime
            t.setDateCreated(new Date());
            t.setLastUpdated(new Date());

            conn = H2.getConnection();

            //generate column_name and column_value
            List<String> columns = new ArrayList<>();//e.g. [date_created, last_updated, email, first_name, last_name]
            List<Object> values = new ArrayList<>(); // each field's value

            for (Field field : fields(clazz)) {
                String column = column(field);
                if (column.equals("id"))
                    continue;
                columns.add(column);
                field.setAccessible(true);
                values.add(field.get(t));
            }

            //generate insert SQL
            //e.g. "INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (?, ?, ?, ?, ?)"
            String sql = generateInsertSQL(columns);

            stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            //set value
            for (int i = 1; i <= values.size(); i++) {
                Object value = values.get(i-1);
                if (value == null) {
                    stmt.setNull(i, 0);
                    continue;
                }
                if (value instanceof Date)
                    stmt.setTimestamp(i, new Timestamp(((Date)value).getTime()));
                else if (value instanceof String)
                    stmt.setString(i, (String)value);
                else if (value instanceof BigDecimal)
                    stmt.setBigDecimal(i, (BigDecimal)value);
                else if (value instanceof Double)
                    stmt.setDouble(i, (Double)value);
                else if (value instanceof Float)
                    stmt.setFloat(i, (Float)value);
                else if (value instanceof Long)
                    stmt.setLong(i, (Long)value);
                else if (value instanceof Integer)
                    stmt.setInt(i, (Integer)value);
                else if (value instanceof Short)
                    stmt.setShort(i, (Short)value);
                else if (value instanceof Integer)
                    stmt.setInt(i, (Integer)value);
                else if (value instanceof Byte)
                    stmt.setByte(i, (Byte)value);
                else if (value instanceof Boolean)
                    stmt.setBoolean(i, (Boolean)value);
                else if (value.getClass().isEnum())
                    stmt.setString(i,value.toString());

            }

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                logger.severe("Creating failed." + t);
                throw new DatabaseException(CREATE_ROW_IN_DB);
            }

            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                logger.severe("Creating failed, no Id be generated." + t);
                throw new DatabaseException(NO_ID_BE_GENERATED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DatabaseException(CREATE_ROW_IN_DB);
        } finally {
            close(conn,stmt,generatedKeys);
        }

    }

    /**
     * Generate Insert SQL
     *
     * e.g.
     *   "INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (?, ?, ?, ?, ?)"
     *
     * @param columns: column name List
     * @return SQL
     */
    private String generateInsertSQL(List<String> columns){
        StringBuffer sb = new StringBuffer();       //date_created, last_updated, email, first_name, last_name
        StringBuffer sbValue = new StringBuffer();  //?, ?, ?, ?, ?

        for (String column : columns) {
            if (sb.length() > 1){
                sb.append(",");
                sbValue.append(",");
            }
            sb.append(column);
            sbValue.append("?");
        }

        String sql = sql_insert + sb.toString() + ") VALUES (" + sbValue.toString() + ");";//"INSERT INTO User (date_created, last_updated, email, first_name, last_name) VALUES (?, ?, ?, ?, ?)"

        return sql;
    }

    /**
     * get all fields of the Class, include its parent class's fields.
     *
     * @param clazz: Class
     * @return Filed List
     */
    private List<Field> fields(Class clazz){
        List<Field> fields = new ArrayList<>();
        Class c = clazz;
        while (!c.equals(Object.class)){
            for (Field field : c.getDeclaredFields()) {
                fields.add(field);
            }
            c = c.getSuperclass();//continue to set super class's field
        }
        return fields;
    }

    /**
     * Select instance from database
     *
     * 1. get row from database
     * 2. generate a new instance by the row
     *
     * @param id
     * @return
     */
    @Override
    public T findById(long id) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        T t = null;
        try {
            conn = H2.getConnection();
            stmt = conn.prepareStatement(sql_select_by_id);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if (rs.next()) {
                t = newInstance(rs);
            }
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(conn, stmt, rs);
        }
    }

    /**
     * Select all instance from database
     *
     * 1. select rows from database
     * 2. generate instances by rows
     *
     * @param offset:
     * @param limit:
     * @return instance list
     */
    @Override
    public List<T> findAll(final Integer offset, final Integer limit) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<T> ts = new ArrayList<>();
        try {
            conn = H2.getConnection();
            stmt = conn.prepareStatement(sql_select_all);
            stmt.setLong(1, offset);
            stmt.setLong(2, limit);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ts.add(newInstance(rs));
            }
            return ts;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            close(conn, stmt, rs);
        }

    }

    /**
     * Get table name from the name of class's XmlRootElement
     */
    private String tableName(Class<T> clazz) {
        return ((Table) clazz.getAnnotation(Table.class)).name();
    }

    /**
     * get database's column
     */
    private String column(Field field){
        Column column = field.getAnnotation(Column.class);
        if (column != null){
            return column.name();
        }else
            return field.getName();
    }

    /**
     * new an instance by row of database.
     *
     * 1. create a new instance
     * 2. loop class's field
     *  2.1 get column
     *  2.2 get column value
     *  2.3 invoke instance's set method
     * 3. return
     *
     * @param rs: row of the database
     */
    protected T newInstance(ResultSet rs) throws IllegalAccessException, InstantiationException, SQLException, InvocationTargetException, NoSuchMethodException {
        T t = clazz.newInstance();

        for (Field field : fields(clazz)) {
            //get column value
            String column = column(field);
            Object valueDb = rs.getObject(column);
            if (field.getType().isEnum())
                valueDb = field.getType().getMethod("valueOf", String.class).invoke(null, valueDb);
            //set value into instance.
            BeanUtils.setProperty(t,field.getName(),valueDb);
        }

        return t;
    }


    protected void close(AutoCloseable... closeables){
        for (AutoCloseable closeable : closeables) {
            if (closeable != null)
                try {
                    closeable.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}
