package com.revolut.dummy.dao;

import com.revolut.dummy.dao.h2.impl.AccountDaoImpl;
import com.revolut.dummy.dao.h2.impl.TransactionDaoImpl;
import com.revolut.dummy.dao.h2.impl.UserDaoImpl;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Dao Factory
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public class DaoFactory {

    private static final Map<Class, Dao> cache = new HashMap<Class, Dao>();

    static {
        cache.put(User.class, new UserDaoImpl());
        cache.put(Account.class, new AccountDaoImpl());
        cache.put(Transaction.class, new TransactionDaoImpl());
    }

    /**
     * get Dao by class.
     *
     */
    public static <T> Dao<T> get(final Class clazz){
        return cache.get(clazz);
    }
}
