package com.revolut.dummy.dao;

import java.util.List;

/**
 *
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public interface Dao<T> {
    T findById(final long id);

    long create(T t);

    List<T> findAll(Integer offset, Integer limit);
}
