package com.revolut.dummy.dao.user;

import com.revolut.dummy.dao.Dao;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;

/**
 * Transaction DAO
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public interface TransactionDao extends Dao<Transaction> {

    /**
     * Update Transaction's Status
     *
     * @param id : Transaction id
     * @param status: new status
     * return: update true or false
     */
    boolean setStatus(long id, Transaction.Status status);
}