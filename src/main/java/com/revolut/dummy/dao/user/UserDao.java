package com.revolut.dummy.dao.user;

import com.revolut.dummy.dao.Dao;
import com.revolut.dummy.model.User;

/**
 * User DAO
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public interface UserDao extends Dao<User> {

}