package com.revolut.dummy.dao.user;

import com.revolut.dummy.dao.Dao;
import com.revolut.dummy.model.Account;
import com.revolut.dummy.model.Transaction;
import com.revolut.dummy.model.User;

import java.math.BigDecimal;

/**
 * Account DAO
 *
 * @author Zilong.Wang
 *
 * 19-01-2019
 */
public interface AccountDao extends Dao<Account> {

    /**
     * transfer money from Sender's frozenBalance to Receiver's balance.
     *
     */
    boolean doTransfer(Transaction transaction);


}