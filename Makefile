run:
    clean package -Dlog4j.configuration=file:./conf/log4j.properties jetty:run

post_user:
    curl -X POST "http://127.0.0.1:8080/api/user" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"email\": \"1@1.com\", \"firstName\": \"zilong\", \"lastName\": \"Wang\"}"

get_user:
    curl -X GET "http://127.0.0.1:8080/api/user/1" -H "accept: application/json" -H "Content-Type: application/json"

get_all_users:
    curl -X GET "http://127.0.0.1:8080/api/user" -H "accept: application/json" -H "Content-Type: application/json"

get_account:
    curl -X GET "http://127.0.0.1:8080/api/account/1" -H "accept: application/json" -H "Content-Type: application/json"

get_all_accounts:
    curl -X GET "http://127.0.0.1:8080/api/account" -H "accept: application/json" -H "Content-Type: application/json"

get_transaction:
    curl -X GET "http://127.0.0.1:8080/api/transaction" -H "accept: application/json" -H "Content-Type: application/json"

transfer:
    curl -X POST "http://127.0.0.1:8080/api/transaction" -H "Content-Type: application/x-www-form-urlencoded" -d "sender=1&receiver=2&currency=GBP&amount=1.00"







